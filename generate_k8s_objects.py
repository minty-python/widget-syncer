import yaml
import base64

for namespace in ["gov0", "gov1", "com"]:
    config_file = f"helm/{namespace}-config.ini"
    with open(config_file, "rb") as f:
        contents = f.read()
        encoded = base64.b64encode(contents)
        base64_encoded = encoded.decode("utf-8")
    secret = {
        "apiVersion": "v1",
        "kind": "Secret",
        "metadata": {"name": f"{namespace}-zaaksysteem-widget-syncer-config"},
        "type": "Opaque",
        "data": {"config.ini": base64_encoded},
    }
    with open(f"helm/{namespace}-secret.yaml", "w") as f:
        yaml.dump(secret, f)

    cron = {
        "apiVersion": "batch/v1",
        "kind": "CronJob",
        "metadata": {"name": f"widget-syncer-{namespace}"},
        "spec": {
            "schedule": "0 * * * *",
            "jobTemplate": {
                "spec": {
                    "template": {
                        "spec": {
                            "containers": [
                                {
                                    "name": f"widget-syncer-{namespace}",
                                    "image": "barniebarnie/widget-syncer:latest",
                                    "imagePullPolicy": "Always",
                                    "volumeMounts": [
                                        {
                                            "mountPath": "/etc/app/config.ini",
                                            "subPath": "config.ini",
                                            "name": f"{namespace}-rds-config",
                                        }
                                    ],
                                    "env": [
                                        {
                                            "name": "PYTHONUNBUFFERED",
                                            "value": '1'
                                        }
                                    ]
                                }
                            ],
                            "volumes": [
                                {
                                    "name": f"{namespace}-rds-config",
                                    "secret": {
                                        "secretName": f"{namespace}-zaaksysteem-widget-syncer-config"
                                    },
                                }
                            ],
                            "restartPolicy": "Never",
                        }
                    }
                }
            },
            "successfulJobsHistoryLimit": 1,
            "concurrencyPolicy": "Forbid",
        },
    }

    with open(f"helm/{namespace}-cron.yaml", "w") as f:
        yaml.dump(cron, f)
