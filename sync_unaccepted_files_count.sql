BEGIN;

with zaken as (
    select
        distinct case_id
    from
        file
    where
        age(now(), date_created) < '01:15:00'
        and case_id is not null
)
insert into
    queue (status, type, label, priority, data)
select
    'pending',
    'touch_case',
    'devops sql queue item',
    900,
    '{"case_number":"' || z.case_id || '"}'
from
    zaken z
    LEFT JOIN zaak_meta zm ON zm.zaak_id = z.case_id
    LEFT JOIN object_data od ON od.object_id = z.case_id
where
    zm.unaccepted_files_count != (
        od.index_hstore :: hstore -> 'case.num_unaccepted_files'
    ) :: integer;

COMMIT;