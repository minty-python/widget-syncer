BEGIN;

with zaken as (
    select
        distinct t.case_id
    from
        thread_message tm
        LEFT JOIN thread t ON tm.thread_id = t.id
    where
        age(now(), tm.created) < '01:15:00'
        and t.case_id is not null
)
insert into
    queue (status, type, label, priority, data)
select
    'pending',
    'touch_case',
    'devops sql queue item',
    900,
    '{"case_number":"' || z.case_id || '"}'
from
    zaken z
    LEFT JOIN zaak_meta zm ON zm.zaak_id = z.case_id
    LEFT JOIN object_data od ON od.object_id = z.case_id
where
    zm.unread_communication_count != (
        od.index_hstore :: hstore -> 'case.num_unread_communication'
    ) :: integer;

COMMIT;