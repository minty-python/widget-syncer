from helpers import Config, Connection
from time import time
import psycopg2
from pathlib import Path

config = Config()
databases = Connection(config).list_databases()

script_path = Path('/etc/app')
communication_path = script_path / 'sync_unaccepted_communication_count.sql'
files_path = script_path / 'sync_unaccepted_files_count.sql'
cases_path = script_path / 'sync_v0_v1_cases.sql'

with open(communication_path, 'r') as comm, open(files_path, 'r') as files, open(cases_path, 'r') as cases:
    communication_query = comm.read()
    files_query = files.read()
    cases_query = cases.read()

def run_queries(db):
    conn = Connection(config, db=db)
    now = time()
    try:
        print(f"Running communication_query on {db}")
        conn.query(communication_query)
    except (psycopg2.errors.UndefinedColumn, psycopg2.errors.InFailedSqlTransaction, psycopg2.OperationalError):
        pass
    try:
        print(f"Running files_query on {db}")
        conn.query(files_query)
    except (psycopg2.errors.UndefinedColumn, psycopg2.errors.InFailedSqlTransaction, psycopg2.OperationalError):
        pass
    try:
        print(f"Running cases_query on {db}")
        conn.query(cases_query)
    except (psycopg2.errors.UndefinedColumn, psycopg2.errors.InFailedSqlTransaction, psycopg2.OperationalError):
        pass
    post = round((time() - now), 2)
    print(f"Queries on {db} took {post} seconds")
    conn.close()


for db in databases:
    run_queries(db)
