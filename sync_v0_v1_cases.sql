BEGIN;

with zaken as (
    select
        id,
        uuid,
        status,
        route_ou,
        route_role
    from
        zaak
    where
        age(now(), last_modified) < '01:15:00'
)
insert into
    queue (status, type, label, priority, data)
select
    'pending',
    'touch_case',
    'devops sql queue item',
    900,
    '{"case_number":"' || z.id || '"}'
from
    zaken z
    LEFT JOIN object_data od ON z.uuid = od.uuid
    LEFT JOIN zaak_meta zm ON zm.zaak_id = z.id
where
    z.status != od.index_hstore -> 'case.status'
    OR z.route_ou :: text != od.index_hstore :: hstore -> 'case.route_ou'
    OR z.route_role :: text != od.index_hstore :: hstore -> 'case.route_role'
    OR zm.unaccepted_attribute_update_count != (
        od.index_hstore :: hstore -> 'case.num_unaccepted_updates'
    ) :: integer;

COMMIT;