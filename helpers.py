from configparser import ConfigParser
import psycopg2 as pg2


class Config:
    def __init__(self) -> None:
        self.config = ConfigParser()
        CONFIG_PATH = "/etc/app/config.ini"
        with open(CONFIG_PATH, "r") as f:
            self.config.read_file(f)
        self.db_config = self.config["DATABASE"]
        self.host = self.db_config["host"]
        self.port = self.db_config["port"]
        self.pw = self.db_config["pw"]
        self.user = self.db_config["user"]


class Connection:
    def __init__(self, config: Config, db="template1"):
        self.config = config
        self.host = self.config.host
        self.port = self.config.port
        self.pw = self.config.pw
        self.user = self.config.user
        self.db = db

        self.conn = pg2.connect(
            host=self.host,
            database=self.db,
            port=self.port,
            password=self.pw,
            user=self.user
        )

        self._cursor = self.conn.cursor()

    def query(self, query: str) -> list:
        self._cursor.execute(query)

    def list_databases(self):
        self._cursor.execute(
            "SELECT datname FROM pg_database where datconnlimit=-1 and datdba!=10 and datname not in ('zaaksysteem_template', 'template1', 'postgres', 'template0');"
        )
        result = [db[0] for db in self._cursor.fetchall()]
        self.close()
        return result

    def close(self):
        self.conn.close()
        self._cursor.close()
