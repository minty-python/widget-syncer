FROM python:3.9-slim-bullseye AS base

COPY requirements.txt /tmp/requirements.txt

RUN python3 -m venv /opt/virtualenv \
  && . /opt/virtualenv/bin/activate \
  && pip install -r /tmp/requirements.txt

ENV PATH="/opt/virtualenv/bin:$PATH"

COPY main.py *.sql helpers.py /etc/app/

WORKDIR /etc/app

CMD ["python3.9", "main.py"]